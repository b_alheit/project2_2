#ifndef PROJECT2_2DPOINT_H
#define PROJECT2_2DPOINT_H

#include <iostream>
using namespace std;

/**
 * \class Point
 * \brief A data type representing a point in \f$ \mathbb{R}^2 \f$.
 */
class Point{
    /**
     * \brief Display functionality for a point
     * @param out iostream cout variable
     * @param pnt The point to displayed
     */
    friend ostream& operator<<(ostream& out, const Point& pnt);

public:
    /**
     * \brief Default constructor. Set x and y value of point to (0,0)
     */
    Point();
    /**
     * \brief Constructor for point.
     * @param initX Initial x value for point
     * @param initY Initial y value for point
     */
    Point(double initX, double initY);
    /**
     * \brief Change the x position of the point
     * @param newX The new x position of the point
     */
    void set_x(double newX);
    /**
     * \brief Change the y position of the point
     * @param newY The new y position of the point
     */
    void set_y(double newY);
    /**
     * \brief Extract the x position of the point
     * @return The x position of the point
     */
    double get_x();
    /**
     * \brief Extract the y position of the point
     * @return The y position of the point
     */
    double get_y();
    /**
     * \brief Method to display the coordinates of a point. (Redundant now due to << display functionality)
     */
    void display();

private:
    double x; /*!< \brief The x position of the point*/
    double y; /*!< \brief The y position of the point*/
};

#endif //PROJECT2_2DPOINT_H
