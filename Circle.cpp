#include "Circle.h"
#include <cmath>

Circle::Circle() {
    centre = Point(0, 0); // Setting the center of the circle to (0,0)
    r = 0; // Setting the radius of the circle to 0
}

Circle::Circle(Point centreInit, double rInit, unsigned long p1, unsigned long p2, unsigned long p3) {
    // Setting the elements in the parents array to value of the indecies of the parents of the circles
    parents[0] = p1;
    parents[1] = p2;
    parents[2] = p3;
    centre = centreInit; // Setting the center of the circle to the input point
    r = rInit; // Setting the radius of the circle to the input radiusf
}

Circle::Circle(Point centreInit, double rInit) {
    centre = centreInit; // Setting the center of the circle to the input point
    r = rInit; // Setting the radius of the circle to the input radius
}

Circle::Circle(Point p1, Point p2, Point p3) {
    Vector2D c1(p1), c2(p2), c3(p3); // Creating three vectors with components with the values of input points

    // Applying equations to calculate the position of the center of the circle and the radius of the circle
    double Y13 = c1[1] - c3[1];
    double Y23 = c2[1] - c3[1];
    double X13 = c1[0] - c3[0];
    double X23 = c2[0] - c3[0];
    double P13 = pow(c1.norm(),2) - pow(c3.norm(),2);
    double P23 = pow(c2.norm(),2) - pow(c2.norm(),2);

    centre.set_x((Y13*P23 - Y23*P13) / (2*(X23*Y13 - X13*Y23))); // Setting the x position of the center of the circle
    centre.set_y((P23 - 2*X23*centre.get_x())/(2*Y23)); // Setting the y position of the center of the circle
    r = sqrt(pow((c1[0]-centre.get_x()),2)+pow((c1[1]-centre.get_y()),2)); // Setting the radius of the circle
}

Point Circle::get_centre() {return centre;} // Return the center of the circle

double Circle::get_r() {return r;} // Return the radius of the circle


unsigned long Circle::get_parents(int i) {
    return parents[i]; // Return the index of the parent in the gasket's vector of circles
}

void Circle::plot(Plotter &plotter) {
    plotter.fcircle(centre.get_x(),centre.get_y(),r); // Using the plotter library to plot the circle to an svg file
}