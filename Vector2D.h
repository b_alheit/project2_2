#ifndef PROJECT2_VECTOR2D_H
#define PROJECT2_VECTOR2D_H

#include "Point.h"
#include <iostream>
using namespace std;
/**
 * \class Vector2D
 * \brief A data type representing a vector in \f$ \mathbb{R}^2 \f$.
 *
 */
class Vector2D {
    /**
     * \brief Output display functionality
     * @param out iostream variable
     * @param vec The vector to be displayed
     */
    friend ostream& operator<<(ostream& out, const Vector2D& vec);
    /**
	 * \brief Input via terminal functionality
	 * @param in iostream variable
	 * @param vec The vector to be assigned values to
	 */
    friend istream& operator>>(istream& in, Vector2D& vec);
    /**
	 * \brief Adds two vectors component-wise
	 * @param lhs The vector on the left-hand side of the operator
	 * @param rhs The vector on the right-hand side of the operator
	 * @return A vector that is the result of vector addition of the input vectors
	 */
    friend Vector2D operator+(const Vector2D& lhs, const Vector2D& rhs);
    /**
	 * \brief Subtracts to vectors component-wise
	 * @param lhs The vector on the left-hand side of the operator
	 * @param rhs The vector on the right-hand side of the operator
	 * @return The resulting vector from the vector subtraction of the lhs vector by the rhs vector
	 */
    friend Vector2D operator-(const Vector2D& lhs, const Vector2D& rhs);
    /**
	 * \brief Dot product operator
	 * @param lhs The vector on the left-hand side of the operator
	 * @param rhs The vector on the right-hand side of the operator
	 * @return The dot product of the two vectors
	 */
    friend double operator*(const Vector2D& lhs, const Vector2D& rhs);
    /**
	 * \brief Scalar multiplier
	 * @param lhs Vector on the left-hand side of the operator
	 * @param rhs Scalar on the right-hand side of the operator
	 * @return The input vector with each of its components multiplied by the input scalar
	 */
    friend Vector2D operator*(const Vector2D& lhs, const double& rhs);
    /**
	 * \brief Scalar multiplier
	 * @param lhs Scalar on the left-hand side of the operator
	 * @param rhs Vector on the right-hand side of the operator
	 * @return The input vector with each of its components multiplied by the input scalar
	 */
    friend Vector2D operator*(const double& lhs, const Vector2D& rhs);
    /**
	 * \brief Test for equality. Returns True if both elements in each vector are equal to the corresponding element in the other vector
	 * @param lhs Vector on the left-hand side of the operator
	 * @param rhs Vector on the right-hand side of the operator
	 * @return Boolean denoting equality of the vectors
	 */
    friend bool operator==(const Vector2D& lhs, const Vector2D& rhs);
    /**
	 * \brief Test for inequality. Returns True if either element in each vector is not equal to the corresponding element in the other vector
	 * @param lhs Vector on the left-hand side of the operator
	 * @param rhs Vector on the right-hand side of the operator
	 * @return Boolean denoting inequality of the vectors
	 */
    friend bool operator!=(const Vector2D& lhs, const Vector2D& rhs);

public:
    /**
     * \brief Default constructor for Vector2D class. Sets x and y components to 0.
     */
    Vector2D();
    /**
     * \brief Constructor for Vector2D class.
     * @param xInit Initial x component for vector
     * @param yInit Initial y component for vector
     */
    Vector2D(double xInit, double yInit);
    /**
     * \brief Constructor for Vector2D class. Sets the x and y components to the value of the x and y position of the point that it is given
     * @param p Point to create vector components out of
     */
    Vector2D(Point p);
    /**
     * \brief Calculates the norm of the vector
     * @return The norm of the vector
     */
    double norm();
    /**
     * \brief Finds to maximum component of the vector
     * @return The maximum component of the vector
     */
    double max();
    /**
     * \brief Finds the minimum component of the vector
     * @return The minimum component of the vector
     */
    double min();
    /**
     * \brief Subscript operator functionality
     * @param i The index of the desired component (indexing starts from 0).
     * @return A reference to the component at that index
     */
    double&operator[](const int i);

private:
    double x; /*!< \brief The x component of the vector*/
    double y; /*!< \brief The y component of the vector*/
};


#endif //PROJECT2_VECTOR2D_H
