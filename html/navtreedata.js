var NAVTREE =
[
  [ "Apollonian Gasket", "index.html", [
    [ "Limitations", "limitations.html", null ],
    [ "User manual", "md_doc_manual.html", null ],
    [ "Problem statement", "md_doc_problem_statment.html", null ],
    [ "Disscusion of theory", "md_doc_theory.html", null ],
    [ "Verification and validation", "md_doc_verification.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_c_make_c_compiler_id_8c.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';