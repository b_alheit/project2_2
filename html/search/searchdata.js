var indexSectionsWithContent =
{
  0: "acdefghilmnopstuv",
  1: "cgpv",
  2: "cfglmptv",
  3: "cdegimnopstv",
  4: "fi",
  5: "o",
  6: "acdhps",
  7: "adlpuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "related",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Friends",
  6: "Macros",
  7: "Pages"
};

