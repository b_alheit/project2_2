//
// Created by cerecam on 5/14/18.
//

#include "Geometry.h"
#include <cmath>
using namespace std;

bool exTangent(Circle& c1, Circle& c2) {
    // Extract the centers and radii from the two circles
    double x1 = c1.get_centre().get_x();
    double y1 = c1.get_centre().get_y();
    double r1 = c1.get_r();

    double x2 = c2.get_centre().get_x();
    double y2 = c2.get_centre().get_y();
    double r2 = c2.get_r();

    // If the percentage difference between the sum of the radii and the distance from the centres is sufficiently small return true
    return (abs((r1 + r2 - sqrt(pow(x1-x2,2) + pow(y1-y2,2)))/(r1 + r2 )) < 0.0001);
}

bool intTangent(Circle& c1, Circle& c2){
    // Extract the centers and radii from the two circles
    double x1 = c1.get_centre().get_x();
    double y1 = c1.get_centre().get_y();
    double r1 = c1.get_r();

    double x2 = c2.get_centre().get_x();
    double y2 = c2.get_centre().get_y();
    double r2 = c2.get_r();

    // If the percentage difference between the difference of the radii and the distance from the centres is sufficiently small return true
    return (abs((abs(r1 - r2) - sqrt(pow(x1-x2,2) + pow(y1-y2,2)))/((r1 + r2)/2)) < 0.0001);
}

bool tangent(Circle& c1, Circle& c2){
    // If the circles are internally tangent or externally tangent return True
    return intTangent(c1, c2) || exTangent(c1, c2);
}

bool in(Circle& c1, Circle& c2) {
    // Extract values from the circles
    double x1 = c1.get_centre().get_x();
    double y1 = c1.get_centre().get_y();

    double x2 = c2.get_centre().get_x();
    double y2 = c2.get_centre().get_y();
    double r2 = c2.get_r();

    // If the distance between the first and second circle's centers is less than the radius of the second circle return true
    return sqrt(pow(x1-x2,2) + pow(y1-y2,2)) < r2;
}

bool in(double& x, double& y, Circle& c){
    double x2 = c.get_centre().get_x();
    double y2 = c.get_centre().get_y();
    double r2 = c.get_r();

    // If the distance between point and circle's center is less than the radius of the circle return true
    return sqrt(pow(x-x2,2) + pow(y-y2,2)) < r2;
}

bool same(Circle c1, Circle c2){
    // If the difference between the two circles centers x and y positions and the difference between the two circle's radii is sufficiently small return True
    return (abs(c1.get_centre().get_x() - c2.get_centre().get_x()) < 0.0001 &&
            abs(c1.get_centre().get_y() - c2.get_centre().get_y()) < 0.0001 &&
            abs(c1.get_r() - c2.get_r()) < 0.0001);
}