//
// Created by cerecam on 5/14/18.
//

#ifndef PROJECT2_GEOMETRY_H
#define PROJECT2_GEOMETRY_H

#include "Circle.h"
#include <complex>

/**
 * \brief Determines if two circles are externally tangent
 * @param c1 The one circle
 * @param c2 The other circle
 * @return True if they are externally tangent, False if they are not
 */
bool exTangent(Circle& c1, Circle& c2);
/**
 * \brief Determines if two circles are internally tangent
 * @param c1 The one circle
 * @param c2 The other circle
 * @return True if they are internally tangent, False if they are not
 */
bool intTangent(Circle& c1, Circle& c2);
/**
 * \brief Determines if two circles are tangent
 * @param c1 The one circle
 * @param c2 The other circle
 * @return True if they are tangent, False if they are not
 */
bool tangent(Circle& c1, Circle& c2);
/**
 * \brief Determines if the center of circle c1 is inside of circle c2
 * @param c1 The contained circle
 * @param c2 The containing circle
 * @return True if c1's center is inside of c2, False if it is not
 */
bool in(Circle& c1, Circle& c2);
/**
 * \brief Determines if a point is inside of a circle
 * @param x The x position of the point
 * @param y The y position of the point
 * @param c The circle that may or may not contain the point
 * @return True if the point lies inside of the circle, False if it does not
 */
bool in(double& x, double& y, Circle& c);
/**
 * \brief Determines if one circle is the same as another circle
 * @param c1 The one circle
 * @param c2 The other circle
 * @return True if c1 has the same center and radius as c2, False if not
 */
bool same(Circle c1, Circle c2);


#endif //PROJECT2_GEOMETRY_H
