//
// Created by cerecam on 5/14/18.
//
#include <cmath>
#include "Gasket.h"
#include "Geometry.h"
#include <complex>

using namespace std;

Gasket::Gasket() {// create empty gasket object
    }

Gasket::Gasket(Circle c1, Circle c2, Circle c3) {
    // If one of the circles are not tangent to another print an error message and leave the circles vector empty to induce an error later
    if(!(tangent(c1, c2) && tangent(c2, c3) && tangent(c3, c1))){
        cerr << "The inputted circles are not mutually tangent. Please try again" << endl;
    }else{
        // If all of the circles are tangent to each other then add them to the vector of circles
        circles.push_back(c1);
        circles.push_back(c2);
        circles.push_back(c3);
    }
}

void Gasket::inputCircles() {
    // Initialise variables to define circles
    double x1, x2, x3, y1, y2, y3, r1, r2, r3;

    // Prompt user to get values to efine circles
    cout << "Welcome to the Apollonian Gasket generator" << endl;
    cout << "Please enter the x position of the center of the first circle:" << endl;
    cin >> x1;
    cout << "Please enter the y position of the center of the first circle:" << endl;
    cin >> y1;
    cout << "Please enter the radius of the first circle:" << endl;
    cin >> r1;
    Circle c1(Point(x1, y1), r1);
    cout << "Please enter the x position of the center of the second circle:" << endl;
    cin >> x2;
    cout << "Please enter the y position of the center of the second circle:" << endl;
    cin >> y2;
    cout << "Please enter the radius of the second circle:" << endl;
    cin >> r2;
    Circle c2(Point(x2, y2), r2);
    cout << "Please enter the x position of the center of the third circle:" << endl;
    cin >> x3;
    cout << "Please enter the y position of the center of the third circle:" << endl;
    cin >> y3;
    cout << "Please enter the radius of the third circle:" << endl;
    cin >> r3;
    Circle c3(Point(x3, y3), r3);

    // Check to see if all of the circles are tangent to each other
    if(tangent(c1, c2) && tangent(c2, c3) && tangent(c3, c1)){
        // Check to see that none of the circles are the same circle as another
        if(!(same(c1,c2) || same(c2, c3) || same(c3, c1))){
            // If valid circles, add them to the vector of circles
            circles.push_back(c1);
            circles.push_back(c2);
            circles.push_back(c3);
        } else{
            // If one of the circles is the same as another, inform user and recurse
            cout << "At least two of the inputted circles are the same. " << endl
               << "Three different but mutually tangent circles are required. "
               << "Please try again." << endl;
            inputCircles();
        }
    } else{
        // If at least one of the circles is not tangent to another, inform user and recurse
        cout << "At least one pair of the inputted circles are not tangent to each other. " << endl
             << "Three different but mutually tangent circles are required. "
             << "Please try again." << endl;
        inputCircles();
    }
}

void Gasket::generate(const int& n) {
    unsigned long start = 3; // The index at which the first child will lie after the first iteration
    unsigned long nCircles; // Initialise variable to store the number of circles before the new children were made
    for(int i = 1; i < n; i++){
        nCircles = circles.size(); // Determine the number of circles before the children are made
        if(i==1){
            makeChild(0, 1, 2); // If this is the first iteration make children only using the initial triple
        }
        else {
            // If not first iteration, loop through children to make new children with all possible combinations of their parents (ew)
            for(unsigned long j = start; j < nCircles; j++){
                makeChild(j, circles[j].get_parents(0), circles[j].get_parents(1));
                makeChild(j, circles[j].get_parents(1), circles[j].get_parents(2));
                makeChild(j, circles[j].get_parents(2), circles[j].get_parents(0));
            }
            start = nCircles; // Set the new start to be the number of circles before the new children were made
        }
    }
}

void Gasket::makeChild(const unsigned long& p1, const unsigned long& p2, const unsigned long& p3) {

    int mult[3] = {1, 1, 1}; // create array of multiplies for positive or negative curvature

    // If one circle contains another make its multiplier negative
    if(in(circles[p1], circles[p2])){
        mult[1] = -1;
    }
    if(in(circles[p1], circles[p3])){
        mult[2] = -1;
    }
    if(in(circles[p2], circles[p1])){
        mult[0] = -1;
    }

    double k1, k2, k3, k4, k4s; // Initiate variables for curvatures

    // Calculate curvatures of three known circles
    k1 = mult[0]/circles[p1].get_r();
    k2 = mult[1]/circles[p2].get_r();
    k3 = mult[2]/circles[p3].get_r();

    // Calculate two possible curvatures for the fourth circle
    k4 = k1 + k2 + k3 + 2*sqrt(abs(k1*k2 + k2*k3 + k3*k1));
    k4s = k1 + k2 + k3 - 2*sqrt(abs(k1*k2 + k2*k3 + k3*k1));

    // Create complex numbers for centers of the three known circles
    complex<double> z1(circles[p1].get_centre().get_x(), circles[p1].get_centre().get_y()),
            z2(circles[p2].get_centre().get_x(), circles[p2].get_centre().get_y()),
            z3(circles[p3].get_centre().get_x(), circles[p3].get_centre().get_y());

    // Calculate the four possible centers
    complex<double> zz1 = (k1*z1 + k2*z2 + k3*z3 + 2.0*sqrt(k1*k2*z1*z2 + k2*k3*z2*z3 + k3*k1*z3*z1)) / k4;
    complex<double> zz2 = (k1*z1 + k2*z2 + k3*z3 - 2.0*sqrt(k1*k2*z1*z2 + k2*k3*z2*z3 + k3*k1*z3*z1)) / k4;
    complex<double> zz3 = (k1*z1 + k2*z2 + k3*z3 + 2.0*sqrt(k1*k2*z1*z2 + k2*k3*z2*z3 + k3*k1*z3*z1)) / k4s;
    complex<double> zz4 = (k1*z1 + k2*z2 + k3*z3 - 2.0*sqrt(k1*k2*z1*z2 + k2*k3*z2*z3 + k3*k1*z3*z1)) / k4s;

    // Create the four possible circles
    Circle cc1(Point(zz1.real(), zz1.imag()), abs(1/k4), p1, p2, p3);
    Circle cc2(Point(zz2.real(), zz2.imag()), abs(1/k4), p1, p2, p3);
    Circle cc3(Point(zz3.real(), zz3.imag()), abs(1/k4s), p1, p2, p3);
    Circle cc4(Point(zz4.real(), zz4.imag()), abs(1/k4s), p1, p2, p3);

    // If any of the circles are tangent to their parents and not the same as one of its parent's parents add it to the vector of circles
    if(tangent(cc1, circles[p1]) && tangent(cc1, circles[p2]) && tangent(cc1, circles[p3]) && !parentsParents(cc1, p1, p2, p3)){circles.push_back(cc1);}
    if(tangent(cc2, circles[p1]) && tangent(cc2, circles[p2]) && tangent(cc2, circles[p3]) && !parentsParents(cc2, p1, p2, p3)){circles.push_back(cc2);}
    if(tangent(cc3, circles[p1]) && tangent(cc3, circles[p2]) && tangent(cc3, circles[p3]) && !parentsParents(cc3, p1, p2, p3)){circles.push_back(cc3);}
    if(tangent(cc4, circles[p1]) && tangent(cc4, circles[p2]) && tangent(cc4, circles[p3]) && !parentsParents(cc4, p1, p2, p3)){circles.push_back(cc4);}
}

void Gasket::plot() {
    // Determine the number of circles
    unsigned long len = circles.size();
    ofstream svgfile; // Create and ofstream object to write to
    svgfile.open ("gasket.svg"); // Open an svg file called gasket.svg
    SVGPlotter plotter(cin, svgfile, cerr); // Create an svg plotter object
    plotter.openpl(); // open the plotter object

    max_min(); // Fill array of max_mins
    double mid_x, mid_y, len_x, len_y, lenp; // Initialise variable for mid points and lengths of plotting areas

    // Calculate mid points and length of plotting areas
    mid_x = (max_mins[0] + max_mins[1]) /2;
    len_x = max_mins[1] - max_mins[0];
    mid_y = (max_mins[2] + max_mins[3]) /2;
    len_y = max_mins[3] - max_mins[2];

    // Determine the maximum length of plotting area
    if(len_x > len_y){lenp = len_x;}
    else{lenp = len_y;}

    // Create space to plot the gasket in
    plotter.fspace(mid_x -lenp/2 - 0.05 ,mid_y - lenp/2 - 0.05, mid_x + lenp/2 + 0.05, mid_y + lenp/2 + 0.05);

    // Set the colour and width of the pen
    plotter.pencolor(0,0,0);
    plotter.flinewidth(.02);

    // Set the fill colour and fill type
    plotter.fillcolor(0,0,0);
    plotter.filltype(0);

    // Loop through all of the circles in the gasket and plot each one
    for(unsigned long i = 0; i < len; i++){
        circles.at(i).plot(plotter);
    }
    plotter.flushpl();
    // Close the plotter
    plotter.closepl();
    // Save and close the .SVG file
    svgfile.close();
}


bool Gasket::parentsParents(const Circle& c, unsigned long p1, unsigned long p2, unsigned long p3) {
    // If all of the parents are the starting three circles return false
    if(p1 < 3 && p2 < 3 && p3 < 3){return False;}
    if(p1 < 3 || p2 < 3 || p3< 3) {
        // If at least one of the parents are one of the starting three
        // Determine the largest parent index
        unsigned long pmax;
        if(p1 < p2){
            pmax = p2;
        } else{
            pmax = p1;
        }
        if(pmax < p3){
            pmax = p3;
        }
        // If a parent is one of the starting three exclude it by taking the maximum parent index instead
        if(p1 < 3){p1 = pmax;}
        if(p2 < 3){p2 = pmax;}
        if(p3 < 3){p3 = pmax;}
    }

    unsigned long parents[3] = {p1, p2, p3};
    int i, j;
    // Loop through the parents of c
    for(i = 0; i < 3; i++){
        // Loop through the parents of parent i of c
        for(j = 0; j < 3; j++){
            // If c is the same as a parent's parent return True
            if(same(circles.at(circles.at(parents[i]).get_parents(j)), c)){
                return True;
            }
        }
    }
    // If c is not the same as any of its parents parents return false
    return False;
}

void Gasket::max_min() {
    // Loop through the first 7 circles (the containing circle will be somewhere in the first seven circles)
    for(int i = 0; i < 7; i++){
        if(i==0){
            // If it is the first iteration then set the max and min values
            max_mins[0] = circles[i].get_centre().get_x() - circles[i].get_r();
            max_mins[1] = circles[i].get_centre().get_x() + circles[i].get_r();
            max_mins[2] = circles[i].get_centre().get_y() - circles[i].get_r();
            max_mins[3] = circles[i].get_centre().get_y() + circles[i].get_r();
        }else{
            // If one value is more than or less than a current max or min respectively then set it as the new max or min
            if(max_mins[0] > circles[i].get_centre().get_x() - circles[i].get_r()){
                max_mins[0] = circles[i].get_centre().get_x() - circles[i].get_r();
            }
            if(max_mins[1] < circles[i].get_centre().get_x() + circles[i].get_r()){
                max_mins[1] = circles[i].get_centre().get_x() + circles[i].get_r();
            }
            if(max_mins[2] > circles[i].get_centre().get_y() - circles[i].get_r()){
                max_mins[2] = circles[i].get_centre().get_y() - circles[i].get_r();
            }
            if(max_mins[3] < circles[i].get_centre().get_y() + circles[i].get_r()){
                max_mins[3] = circles[i].get_centre().get_y() + circles[i].get_r();
            }
        }
    }
}