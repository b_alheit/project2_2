//
// Created by cerecam on 5/14/18.
//

#ifndef PROJECT2_GASKET_H
#define PROJECT2_GASKET_H
#include <vector>
#include "Circle.h"
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <plotter.h>
using namespace std;

class Gasket {
public:
    /**
     * \brief Default constructor for the Gasket class
     */
    Gasket();
    /**
     * \brief Constructor for the Gasket class that assigns the starting triple
     *
     * @param c1 One of the starting circles
     * @param c2 One of the starting circles
     * @param c3 One of the starting circles
     */
    Gasket(Circle c1, Circle c2, Circle c3);
    /**
     * \brief Prompts the user to input the values to define the starting triple and stores them in the circles vector
     */
    void inputCircles();
    /**
     * \brief Generates the gasket by adding appropriate circles to the vector of circles
     * @param n The number of levels to which the gasket must be generated
     */
    void generate(const int& n);
    /**
     * \brief Makes the children circles to three given parents
     * @param p1 The index of parent one in the vector of circles
     * @param p2 The index of parent two in the vector of circles
     * @param p3 The index of parent three in the vector of circles
     */
    void makeChild(const unsigned long& p1, const unsigned long& p2, const unsigned long& p3);
    /**
     * \brief Test to see if a given circle is already in the vector of circles
     * @param c Circle to determine if it is the same as one of its parent's parents
     * @param p1 The index of the first parent of c
     * @param p2 The index of the second parent of c
     * @param p3 The index of the third parent of c
     * @return True if c is the same as one of it's parent's parents, False if not
     */
    bool parentsParents(const Circle& c, unsigned long p1, unsigned long p2, unsigned long p3);
    /**
     * \brief Creates an svg file of all the circles in the vector of circles
     */
    void plot();
    /**
     * \brief Constructs the max_mins array of the class that contains the maximum and minimum values of x and y
     */
    void max_min();


private:
    vector<Circle> circles; /*!< \brief A vector of all the circles in the gasket*/
    double max_mins[4]; /*!< \brief An array containing the min x position at 0, max x position at 1, min y position at 2 and max y position at 3*/

};


#endif //PROJECT2_GASKET_H
