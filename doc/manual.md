User manual
=================

To run the programme the user must double click on the executable "project2". The user will be greeted with message shown
in the figure below.

![the caption](..\doc\figures\greet.png)

The user should then follow the prompts similarly to as is shown in the figure below.

![the caption](..\doc\figures\example.png)

The outputted svg file will be created in the same directory as the executable with the name "gasket.svg". Alternatively,
the circles can be given to the programme by constructing them in the main file similarly to as shown in the figure below

![the caption](..\doc\figures\exampleMain.png)

This allows the user to use mathematical functions and operations to determine the values that define the circle. Once the
values have been filled in as desired, the programme must be complied and run. Again, the outputted svg file will be created
in the same file as the executable with the name "gasket.svg".