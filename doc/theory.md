Disscusion of theory
=================

In this programme Descartes' Theorem was used to find the center and radius of a fourth circle that is tangent to a triple
of mutually tangent circles. Mathematically, Descartes' Theorem states the following

 \f{align*}{
k_1^2 + k_2^2 + k_3^2 + k_4^2 = \frac{1}{2}(k_1 + k_2 + k_3 + k_4)^2.
 \f}

Where \f$ k_i \f$ is the curvature of circle \f$ i\f$. The curvature of a circle is determined by

 \f{align*}{
k = \pm\frac{1}{r}
 \f}

Where \f$ r \f$ is the radius of the circle. The curvature is positive or negative depending on whether the circle contains
the other circles or not. If it does contain the other circles then the curvature is negative, and if it does not contain
the other circles then the curvature is positive. Rearranging Descartes' Theorem to solve for the unknown curvature (\f$ k_4 \f$)
yields

 \f{align*}{
k_4 = k_1 + k_2 + k_3  \pm2\sqrt{k_1k_2 + k_2k_3 + k_3k_1}.
 \f}

 Note that there are two possible solutions for the curvature. If one were to represent the center of a circle with a complex number
 complex number then Complex Descartes' Theorem states

  \f{align*}{
 k_1^2z_1^2 + k_2^2z_2^2 + k_3^2z_3^2 + k_4^2z_4^2 = \frac{1}{2}(k_1z_1 + k_2z_2 + k_3z_3 + k_4z_4)^2.
  \f}

Where \f$ z_i = x_i +iy_i \f$ and \f$ (x_i,y_i) \f$ is the center of circle \f$ i \f$. This equation can be rearranged
to solve for the unknown center (\f$ z_4 \f$)

  \f{align*}{
z_4  = \frac{k_1z_1 + k_2z_2 + k_3z_3 \pm 2 \sqrt{k_1z_1k_2z_2 + k_2z_2k_3z_3  + k_3z_3k_1z_1}}{k_4}.
  \f}

Note that there are two possible solutions for the center of the circle for each curvature solution. Hence, there are
four possible solutions overall. All solutions will need to be calculated and then checked for tangency with the original
three circles.