Problem statement
=================

The programme shall take in the positions of the centers and the radii of three mutually tangent circles as an input from
the user. It shall check for tangency of the three circles and that none of the circles are a duplicate of another circle.
If the input is invalid for whatever reason,     the programme shall inform the user why the input is invalid and repeatedly prompt
the user for the required inputs until the input is valid. The programme shall then take in the number of levels to which
the Apollonian Gasket shall be created. The programme shall output an svg file that contains an image of the created
Apollonian Gasket.