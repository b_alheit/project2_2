Verification and validation
===========
The programme was verified by presenting it with a range of different inputs that cover all the possible features of the triples
that could be given to the code.

The first input was the most simple: three externally tangent circles of equal radii that were aligned in a regular fashion as shown in the figure below.

![the caption](..\doc\figures\eqi_3s.png)

When the code was run to ten levels the result was as shown in the figure below.

![the caption](..\doc\figures\eqi_3.png)

The triple of circles with various radii shown below were inputted to the code to test if the code would work for a generic triple of
mutually tangent circles.

![the caption](..\doc\figures\unequis.png)

When the code was run up to ten levels the output was as is shown below.

![the caption](..\doc\figures\unequi.png)

To test if the code would work when given a triple of circles where one of the circles is internally tangent
(an internally tangent triple) the triple shown below was inputted to the code.

![the caption](..\doc\figures\int_eqis.png)

When the code was run up to ten levels the output was as is shown below.

![the caption](..\doc\figures\int_eqi.png)

Finally, to test if the code would would work for a general internally tangent triple the triple shown below was inputted
to the code

![the caption](..\doc\figures\int_unequis.png)

When the code was run up to ten levels the output was as is shown below.

![the caption](..\doc\figures\int_unequi.png)

These results were all created using the main file as shown in the figure below. Each starting triple was created using one
of the comment blocks in the main file. The comment blocks occur in the same order as the results shown in this document.

![the caption](..\doc\figures\verification.png)
