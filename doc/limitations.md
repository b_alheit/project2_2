Limitations {#limitations}
===========

The most significant limitation of the programme is that it does not supply any suggestions to the user when they are entering
the required values for the circles. Since the tolerance for tangency is quite small, the user will have to enter
values with a large number of significant digits for the programme to accept the inputted circles
as valid. However, this is assuming that the user inputs the relevant values for the circles through the command prompt.
If the user creates the circles in the main file, the user will have access to mathematical functions and operators,
making it easier for the user to generate valid values for the circles, and hence making the strict tolerance for tangency
less limiting. Another limiting factor is that the code will occasionally break for inputs that are near the origin. However, the
user will still be able to create any Apollonian Gasket geometry by translating the position of the inputted triple if the code breaks.
This bug is a result from a check that enforces that same circle is not made twice. Although, this check causes this slight limitation,
it is deemed to be worth it since it increases the efficiency of the code and it drops the size of the outputted svg file
from values of circa 50 MB to 50 kB. It is especially worth it when one takes into account how computationally expensive it
is for a computer to open a 50 MB svg file.
