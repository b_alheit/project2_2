#include <iostream>
#include "Point.h"
#include "Vector2D.h"
#include "Circle.h"
#include <vector>
#include <cmath>
#include "Gasket.h"

using namespace std;

int main() {

    int n; // Initialising variable for number levels to construct gasket to
    Gasket G; // Creating a Gasket object
    G.inputCircles(); // Getting the three initial circles from the user
    cout << "Please enter the number of levels you would like to generate: " << endl;
    cin >> n; // Getting the number of levels to construct the gasket to from the user
    G.generate(n); // Generating the circles in the gasket
    G.plot(); // creating the svg image of the gasket

//    Circle  c1(Point(1,1), 1),
//            c2(Point(-1,1), 1),
//            c3(Point(0,1 - sqrt(2*2- 1)), 1);
//
//  Circle  c1(Point(5, 5), 3),
//            c2(Point(5+5,0+5), 2),
//            c3(Point(3.2+5, -2.4+5), 1);
//
//    Circle  c1(Point(1,0), 1),
//            c2(Point(-1,0), 1),
//            c3(Point(0,0), 2);
//
//  Circle  c1(Point(0.5, 0), 1.5),
//            c2(Point(-1.5, 0), 0.5),
//            c3(Point(0,0), 2);
//
//
//
//     Define the three starting circles. First value is x position, second value is y and third is radius
//    Circle  c1(Point(1,1), 1),
//            c2(Point(-1,1), 1),
//            c3(Point(0,1 - sqrt(2*2- 1)), 1);


    Gasket G(c1, c2, c3);
    G.generate(10); // Insert the number of levels to which the gasket will be generated
    G.plot();

    return 0;
}
