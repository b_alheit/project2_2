#include "Point.h"
using namespace std;

Point::Point() {
    x = 0; // Set point x position to 0
    y = 0; // Set point y position to 0
}

Point::Point(double initX, double initY) {
    x = initX; // Set x position of point to initial input value
    y = initY; // Set y position of point to initial input value
}

void Point::set_x(double newX) {
    x = newX; // Set x position of point to new position
}

void Point::set_y(double newY) {
    y = newY; // Set y position of point to new position
}

void Point::display() {
    cout << "(" << x << "," << y << ")" << endl; // Display to x and y positions of the point in round brackets
}

double Point::get_x() {
    return x; // Return the x position of the point
}

double Point::get_y() {
    return y; // Return the y position of the point
}

ostream& operator<<(ostream &out, const Point &pnt) {
    out << "(" << pnt.x << "," << pnt.y << ")"; // Create iostream cout variable to display the x and y positions of the point in round brackets
    return out;
}